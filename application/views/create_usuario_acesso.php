            <!-- /.row -->
            <div class="row">
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Interfáces de Acesso
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="tabela-interfaces">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Usuario</th>
                                        <th>User Windows</th>
                                        <th>Hostname</th>
                                        <th>CPU</th>
                                        <th>HD</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>1</td>
                                        <td>Nillander</td>
                                        <td>1513_IRON</td>
                                        <td>1513_IRON</td>
                                        <td>BFEBFBFF000906E9</td>
                                        <td>708829020</td>
                                        <td>Ativado</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-check"></i></button>
                                            <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-list"></i></button>
                                            <button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->

               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Cadastrar Novo
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo "<p>".CI_VERSION."</p>";

                                        echo form_open('UsuarioAcesso');

                                            echo '<div class="form-group col-lg-6">';
                                                echo form_label('Nome:', 'nome');
                                                echo form_input('nome', set_value('nome'), array('class' => 'form-control', 'placeholder' => 'Nome'));
                                            echo '</div>';

                                            echo '<div class="form-group col-lg-6">';
                                                echo form_label('Usuário:', 'usuario');
                                                echo form_input('usuario', set_value('usuario'), array('class' => 'form-control', 'placeholder' => 'Usuário'));
                                            echo '</div>';

                                            echo '<div class="form-group col-lg-6">';
                                                echo form_label('Senha:', 'Senha');
                                                echo form_password('Senha', set_value('senha'), array('class' => 'form-control', 'placeholder' => 'Senha'));
                                            echo '</div>';

                                            echo '<div class="form-group col-lg-6">';
                                                echo form_label('Level:', 'level');
                                                $opcoesLevel = array(
                                                    'mesario' => 'Mesário',
                                                    'administrador' => 'Administrador'
                                                );
                                                $htmlStatus = 'class="form-control"';
                                                echo form_dropdown('level', $opcoesLevel, 'mesario', $htmlStatus);
                                            echo '</div>';

                                            echo '<div class="form-group col-lg-12">';
                                            echo '<div class="form-group col-lg-6" style="margin: 0px; padding-left: 0px;>';
                                                echo form_label('Status:', 'level');
                                                $opcoesStatus = array(
                                                    'ativado' => 'Ativado',
                                                    'desativado' => 'Desativado'
                                                );
                                                $htmlStatus = 'class="form-control"';
                                                echo form_dropdown('status', $opcoesStatus, 'ativado', $htmlStatus);
                                            echo '</div>';
                                            echo '</div>';

                                            echo form_submit('cadastrar','Cadastrar', array('class' => 'btn btn-primary'));

                                        echo form_close();
                                    ?>

                                    <form role="form">                                        
                                        
                                        <div class="form-group col-lg-6">
                                            <label>Nome</label>
                                            <input class="form-control" placeholder="Nome">
                                        </div>         

                                        <div class="form-group col-lg-6">
                                            <label>Usuário</label>
                                            <input class="form-control" placeholder="Usuário">
                                        </div>        

                                        <div class="form-group col-lg-6">
                                            <label>Senha</label>
                                            <input class="password form-control" placeholder="Senha">
                                        </div>        

                                        <div class="form-group col-lg-6">
                                            <label>Level</label>
                                            <select class="form-control">
                                                <option>Mesário</option>
                                                <option>Administrador</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-lg-12">
                                            <div class="form-group col-lg-6" style="margin: 0px; padding-left: 0px;">
                                                <label>Status</label>
                                                <select class="form-control">
                                                    <option>Ativado</option>
                                                    <option>Desativado</option>
                                                </select>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                        <button type="reset" class="btn btn-default">Limpar Campos</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

             

            
             
                

            </div>
            <!-- /.row -->

            <script>
    $(document).ready(function() {
        $('#tabela-interfaces').DataTable({
            responsive: true
        });
    });
    </script>