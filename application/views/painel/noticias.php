<style type="text/css">
    form{
        display: grid;
        margin: 10px;
    }

    [type|="file"] {
        margin: 10px 0px;
    }

</style>
<!-- /.row -->
<div class="row"></div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Notícias</div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?
            if($msg = get_msg()):
                echo '<div class="msg-box">'.$msg.'</div>';
            endif;
            switch ($tela) {
                case 'listar':

                if (isset($noticias) && sizeof($noticias) > 0){
                ?>
                <table width="100%" class="table table-striped table-bordered table-hover" id="tabela-interfaces">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Título</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($noticias as $linha):
                    ?>
                        <tr class="odd gradeX">
                            <td><? echo $linha->id; ?></td>
                            <td><? echo $linha->titulo; ?></td>
                            <td>
                                <? echo anchor('noticia/editar/'.$linha->id, 'Editar'); ?> |
                                <? echo anchor('noticia/excluir/'.$linha->id, 'Excluir'); ?> |
                                <? echo anchor('post/'.$linha->id, 'Detalhes', array('target' => '_blank')); ?>
                            </td>
                        </tr>
                    <?
                    endforeach;
                    ?>
                    </tbody>
                </table> <!-- /.table-responsive -->
                <?
                } //if listar
                else {
                    echo '<div class="msg-box"><p>Nenhuma Notícia cadastrada</p></div>';
                }
                break;

                case 'cadastrar':
                ?>
                <div style="display:grid;">
                <?
                echo form_open_multipart();
                echo form_label('Título', 'titulo');
                echo form_input('titulo', set_value('titulo'));
                echo form_label('Conteúdo', 'conteudo');
                echo form_textarea('conteudo', to_html(set_value('conteudo')), array('class' => 'editorhtml'));
                echo form_label('Imagem da notícia (thumbnail):', 'field_imagem');
                echo form_upload('field_imagem');
                echo form_submit('envia', 'Enviar Notícia', array('class' => 'btn', 'class' => 'btn btn-lg btn-success'));
                echo form_close();
                ?>
                </div>
                <?
                break;
                
                case 'excluir':
                ?>
                <div style="display:grid;">
                <?
                echo form_open_multipart();
                echo form_label('Título', 'titulo');
                echo form_input('titulo', set_value('titulo', to_html($noticia->titulo)));
                echo form_label('Conteúdo', 'conteudo');
                echo form_textarea('conteudo', to_html(set_value('conteudo', to_html($noticia->conteudo))), array('class' => 'editorhtml'));
                echo '<p><small>Imagem:</small><br /><img src="'.base_url('uploads/'.$noticia->imagem).'" class="thumb-edicao" /></p>';
                echo form_submit('excluir', 'Excluir Notícia', array('class' => 'btn', 'class' => 'btn btn-lg btn-success'));
                echo form_close();
                ?>
                </div>
                <?
                break;

                case 'editar':
                ?>
                <div style="display:grid;">
                <?
                echo form_open_multipart();
                echo form_label('Título', 'titulo');
                echo form_input('titulo', set_value('titulo', to_html($noticia->titulo)));
                echo form_label('Conteúdo', 'conteudo');
                echo form_label('Imagem da notícia (thumbnail):', 'field_imagem');
                echo form_upload('field_imagem');
                echo form_textarea('conteudo', to_html(set_value('conteudo', to_html($noticia->conteudo))), array('class' => 'editorhtml'));
                echo '<p><small>Imagem Atual:</small><br /><img src="'.base_url('uploads/'.$noticia->imagem).'" class="thumb-edicao" /></p>';
                echo form_submit('salvar', 'Salvar Notícia', array('class' => 'btn', 'class' => 'btn btn-lg btn-success'));
                echo form_close();
                ?>
                </div>
                <?
                break;


                default:
                    echo 'Default';
                break;
            }
            ?>
            </div> <!-- /.panel-body -->
        </div> <!-- /.panel -->
    </div> <!-- /.col-lg-8 -->

    <div class="panel-body">
        <div class="col-lg-6">
            <a href="<?php echo base_url('InterfaceAcesso'); ?>"><button type="button" class="btn btn-primary btn-lg btn-lg">Nova Interface</button></a>
        </div>
    </div>

</div>
<!-- /.row -->

<script>
     $(document).ready(function() {
        $('#tabela-interfaces').DataTable({
            ordering: false,
            responsive: true
        });
    });
</script>
<script>
    $('.editorhtml').jqte();
</script>