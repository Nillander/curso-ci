            <!-- /.row -->
            <div class="row">
                
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Interfáces de Acesso
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="tabela-interfaces">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Usuario</th>
                                        <th>User Windows</th>
                                        <th>Hostname</th>
                                        <th>CPU</th>
                                        <th>HD</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td>1</td>
                                        <td>Nillander</td>
                                        <td>1513_IRON</td>
                                        <td>1513_IRON</td>
                                        <td>BFEBFBFF000906E9</td>
                                        <td>708829020</td>
                                        <td>A</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-circle"><i class="fa fa-check"></i></button>
                                            <button type="button" class="btn btn-primary btn-circle"><i class="fa fa-list"></i></button>
                                            <button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->

               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Cadastrar Nova
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form">                                        
                                        
                                        <div class="form-group col-lg-6">
                                            <label>Nome do Usuário</label>
                                            <input class="form-control" placeholder="Usuário">
                                        </div>         

                                        <div class="form-group col-lg-6">
                                            <label>User Windows do Computador</label>
                                            <input class="form-control" placeholder="User Windows">
                                        </div>        

                                        <div class="form-group col-lg-6">
                                            <label>Hostname do Computador</label>
                                            <input class="form-control" placeholder="Hostname">
                                        </div>        

                                        <div class="form-group col-lg-6">
                                            <label>Número de Série da CPU</label>
                                            <input class="form-control" placeholder="CPU Serial">
                                        </div>    

                                        <div class="form-group col-lg-6">
                                            <label>Número de Série do HD</label>
                                            <input class="form-control" placeholder="HD Serial">
                                        </div>   

                                        <div class="form-group col-lg-6">
                                            <label>Status</label>
                                            <select class="form-control">
                                                <option>Ativado</option>
                                                <option>Desativado</option>
                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                        <button type="reset" class="btn btn-default">Limpar Campos</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

             

            
             
                

            </div>
            <!-- /.row -->

            <script>
    $(document).ready(function() {
        $('#tabela-interfaces').DataTable({
            responsive: true
        });
    });
    </script>