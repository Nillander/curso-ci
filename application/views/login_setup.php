<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Por favor, Entre</h3>
            </div>
            <div class="panel-body">
                <?php 
                if($msg = get_msg()):
                    echo '<div class="msg-box">'.$msg.'</div>';
                endif;
                echo form_open();
                echo form_input('username', set_value('username'), array('autofocus' => 'autofocus', 'class' => 'form-control form-group', 'placeholder' => 'Digite seu Usuário'));
                echo form_password('senha', set_value('senha'), array('class' => 'form-control form-group', 'placeholder' => 'Digite sua Senha'));
                echo form_submit('envia', 'Entrar', array('class' => 'btn', 'class' => 'btn btn-lg btn-success btn-block'));
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>