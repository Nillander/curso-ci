<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InterfaceAcesso extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function index() {
		$dados['titulo_pagina'] = 'Cadastrar Interfáces de Acesso';

		$this->load->view('init_cabecalho', $dados);
		$this->load->view('create_interface_acesso');
		$this->load->view('init_rodape');
		
	}

	public function update(){
		$dados['titulo_pagina'] = 'Editar Interfáces de Acesso';

		$this->load->view('init_cabecalho', $dados);
		$this->load->view('update_interface_acesso');
		$this->load->view('init_rodape');
	}
}
