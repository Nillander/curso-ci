<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('option_model'); //$this->load->model('option_model', 'option');
    }

    public function index() {
        if ($this->option_model->get_option('setup_executado') == 1) {
            redirect('setup/login', 'refresh'); //setup ok, mostrar tela login
        } else {
            redirect('setup/instalar', 'refresh'); //não instalado, mostrar tela de setup
        }
    }

    public function instalar() {
        if ($this->option_model->get_option('setup_executado') == 1) { //setup ok, mostrar tela para editar dados de setup
            redirect('setup/login', 'refresh');
        }
        //regras de validação
        $this->form_validation->set_rules('username', 'USERNAME', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('senha', 'SENHA', 'trim|required|min_length[6]');

        if ($this->form_validation->run() == FALSE) { //verifica a validação
            if (validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $dados_form = $this->input->post();
            $this->option_model->update_option('user_login', $dados_form['username']);
            $this->option_model->update_option('user_password', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            $inserido = $this->option_model->update_option('setup_executado', 1);
            if ($inserido == 1) {
                set_msg('<p>Sistema instalado som sucesso!</p>');
                redirect('setup/login', 'refresh');
            }
        }

        //carrega view
        $this->load->view('painel/init_1head');
        $this->load->view('login_setup');
        $this->load->view('painel/init_rodape');
    }


    public function login() {
        if ($this->option_model->get_option('setup_executado') != 1) { //setup não está ok, mostrar tela para instalar o sistema
            redirect('setup/instalar', 'refresh');
        }
        
        //regras de validação
        $this->form_validation->set_rules('username', 'USERNAME', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('senha', 'SENHA', 'trim|required');

        if ($this->form_validation->run() == FALSE) { //verifica a validação
            if (validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $dados_form = $this->input->post();
            if ($this->option_model->get_option('user_login') == $dados_form['username']) { //usuário existe
                if (password_verify($dados_form['senha'], $this->option_model->get_option('user_password'))) { //senha ok, fazer login
                    $this->session->set_userdata('logged', TRUE);
                    $this->session->set_userdata('user_login', $dados_form['username']);
                    redirect('Dashboard', 'refresh'); //fazer redirect para home do painel
                    //redirect('setup/alterar','refresh'); //fazer redirect para home do painel
                } else {
                    set_msg('<p>Senha incorreta!</p>'); //senha incorreta
                }
            } else {
                set_msg('<p>Usuário inexistente!</p>'); //usuário não existe
            }
        }

        $ci = & get_instance();
        if($ci->session->userdata('logged') == true){
            redirect('Dashboard','refresh');
        }


        //carrega view
        $this->load->view('painel/init_1head');
        $this->load->view('login_setup');
        $this->load->view('painel/init_rodape');
    }

    public function logout() {
        verifica_login();
        $this->session->set_userdata('logged', FALSE);
        $this->session->set_userdata('user_login', null);
        redirect('setup','refresh');
    } // end of method


    public function alterar(){
    	verifica_login(); // verifica login do usuário;
		//regras de validação
        $this->form_validation->set_rules('username', 'USERNAME', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('senha', 'SENHA', 'trim|min_length[6]');
        if(isset($_POST['senha']) && $_POST['senha'] != ''){
        	$this->form_validation->set_rules('senha2', 'REPITA A SENHA', 'trim|required|min_length[6]|matches[senha]');
    	}

        if ($this->form_validation->run() == FALSE) { //verifica a validação
            if (validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $dados_form = $this->input->post();
            var_dump($dados_form);
            $this->option_model->update_option('user_login', $dados_form['username']);

            if(isset($_POST['senha']) && $_POST['senha'] != ''){
            	$this->option_model->update_option('user_password', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            }
			set_msg('Dados alterados com sucesso!');
        }

        //carrega view
    	//$_POST['username'] = $this->option_model->get_option('user_login');
		$dados['titulo_pagina'] = 'Painel de Controle';
    	$dados['h2'] = 'Configuração de Página';
		$this->load->view('painel/init_1head');
    	$this->load->view('painel/config', $dados);
    	$this->load->view('painel/init_rodape');
    }

} // end of file