<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		verifica_login();
	}

	public function index() {
		$dados['titulo_pagina'] = 'Painel de Controle';
		$this->load->view('painel/init_cabecalho', $dados);
		$this->load->view('dashboard_sgpld');
		$this->load->view('painel/init_rodape');
	}

	public function login(){
		$this->load->view('painel/init_1head');
		$this->load->view('login_setup');
		$this->load->view('painel/init_rodape');
	}


}
