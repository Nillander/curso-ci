<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioAcesso extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function index() {
		$this->load->helper('form');
		$dados['titulo_pagina'] = 'Cadastrar Usuários de Acesso';

		$dados_form = $this->input->post();
		var_dump($dados_form);

		$this->load->view('init_cabecalho', $dados);
		$this->load->view('create_usuario_acesso');
		$this->load->view('init_rodape');
		
	}

	public function update(){
		$dados['titulo_pagina'] = 'Editar Usuários de Acesso';

		$this->load->view('init_cabecalho', $dados);
		$this->load->view('update_usuario_acesso');
		$this->load->view('init_rodape');
	}
}
