<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticia extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('option_model');
        $this->load->model('noticia_model');
        verifica_login();
    } // end of method

    public function index() {
    	redirect('noticia/listar','refresh');
    } // end of method

    public function listar(){
    	$dados_cabecalho['titulo_pagina'] = 'Listagem de Notícias';
		$dados['tela'] = 'listar';
		$dados['noticias'] = $this->noticia_model->get();
		$this->load->view('painel/init_cabecalho', $dados_cabecalho);
		$this->load->view('painel/noticias', $dados);
		$this->load->view('painel/init_rodape');
    } // end of method

    public function cadastrar(){
    	$this->form_validation->set_rules('titulo', 'TÍTULO', 'trim|required');
    	$this->form_validation->set_rules('conteudo', 'CONTEÚDO', 'trim|required');

    	if($this->form_validation->run() == FALSE) {
            if(validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $this->load->library('upload', config_upload());
            if($this->upload->do_upload('field_imagem')){ // sucesso
            	$dados_upload = $this->upload->data();
            	$dados_form = $this->input->post();
            	//na esquerda nome da tabela, na direita nome do formulário
            	$dados_insert['titulo'] = to_bancodedados($dados_form['titulo']);
            	$dados_insert['conteudo'] = to_bancodedados($dados_form['conteudo']);
            	$dados_insert['imagem'] = $dados_upload['file_name'];

            	if($id = $this->noticia_model->salvar($dados_insert)){
            		set_msg('<p>Notícia cadastrada com sucesso!</p>');
            		redirect('noticia/editar/'.$id,'refresh');
            	} else {
					set_msg('<p>Cê bugou o trem maluco.</p>');
            	}

            	var_dump($dados_upload);
            } else { // erro no upload
            	$msg = $this->upload->display_errors();
            	$msg .= "<p>São permitidos arquivos JPG e PNG de até 512KB.</p>";
            	set_msg($msg);
            }

        }

		$dados_cabecalho['titulo_pagina'] = 'Cadastro de Notícias';
		$dados['tela'] = 'cadastrar';
		$dados['noticias'] = $this->noticia_model->get();
		$this->load->view('painel/init_cabecalho', $dados_cabecalho);
		$this->load->view('painel/noticias', $dados);
		$this->load->view('painel/init_rodape');
    } // end of method

    public function excluir() {
        if(($id = $this->uri->segment(3)) > 0){
            if($noticia = $this->noticia_model->get_single($id)){
                $dados['noticia'] = $noticia;
            } else {
                set_msg("<p>Notícia Inexistente</p>");
                redirect('noticia','refresh');
            }
        } else {
            set_msg("<p>Você deve escolher uma notícia para excluir</p>");
            redirect('noticia','refresh');
        }

        $this->form_validation->set_rules('excluir', 'EXCLUIR', 'trim|required'); // regras de validação
        if($this->form_validation->run() == FALSE) { // verifica validação
            if(validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $imagem = 'uploads/'.$noticia->imagem;
            if($this->noticia_model->excluir($id)){
                if ($imagem != ''){
                    unlink($imagem);
                }
                set_msg("<p>Notícia excluida</p>");
                redirect('noticia','refresh');
            } else {
                set_msg("<p>Nenhuma notícia foi excluida</p>");
                redirect('noticia','refresh');
            }
        }

        $dados_cabecalho['titulo_pagina'] = 'Exclusão de Notícias';
        $dados['tela'] = 'excluir';
        $dados['noticias'] = $this->noticia_model->get();
        $this->load->view('painel/init_cabecalho', $dados_cabecalho);
        $this->load->view('painel/noticias', $dados);
        $this->load->view('painel/init_rodape');
    } // end of method






    public function editar() {
        if(($id = $this->uri->segment(3)) > 0){ // continuar com a edição
            if($noticia = $this->noticia_model->get_single($id)){
                $dados['noticia'] = $noticia;
                $dados_update['id'] = $noticia->id;
            } else {
                set_msg("<p>Notícia Inexistente. Escolha uma notícia para editar</p>");
                redirect('noticia','refresh');
            }
        } else {
            set_msg("<p>Você deve escolher uma notícia para editar</p>");
            redirect('noticia','refresh');
        }


        // regras de validação
        $this->form_validation->set_rules('titulo', 'TÍTULO', 'trim|required');
        $this->form_validation->set_rules('conteudo', 'CONTEÚDO', 'trim|required');
        
        // verifica validação
        if($this->form_validation->run() == FALSE) {
            if(validation_errors()) {
                set_msg(validation_errors());
            }
        } else {
            $this->load->library('upload', config_upload());
            if(isset($_FILES['field_imagem']) && $_FILES['field_imagem']['name'] != ''){
                // foi enviado uma imagem, deve fazer upload
                if($this->upload->do_upload('field_imagem')){
                    // se o upload foi efetuado
                    $imagem_antiga = 'uploads/'.$noticia->imagem; // armazenar imagem antiga
                    $dados_upload = $this->upload->data(); // armazenar dados de upload
                    $dados_form = $this->input->post(); // armazenar dados do form

                    // alimentar array de dados
                    $dados_update['titulo'] = to_bancodedados($dados_form['titulo']);
                    $dados_update['conteudo'] = to_bancodedados($dados_form['conteudo']);
                    $dados_update['imagem'] = $dados_upload['file_name'];
                    if($this->noticia_model->salvar($dados_update)){
                        unlink($imagem_antiga);
                        set_msg("<p>Notícia alterada com sucesso</p>");
                        $dados['noticia']->imagem = $dados_update['imagem'];
                    } else {
                        set_msg("<p>Nenhuma alteração efetuada</p>");
                    }
                } else {
                    // erro no upload
                    $msg = $this->upload->display_errors();
                    $msg .= "<p>São permitidos arquivos JPG e PNG de até 512KB.</p>";
                    set_msg($msg);
                }
            } else {
                // não foi enviada uma imagem pelo form 
                $dados_form = $this->input->post(); // armazenar dados do form
                // alimentar array de dados
                $dados_update['titulo'] = to_bancodedados($dados_form['titulo']);
                $dados_update['conteudo'] = to_bancodedados($dados_form['conteudo']);
                if($this->noticia_model->salvar($dados_update)){
                    set_msg("<p>Notícia alterada com sucesso</p>");
                } else {
                    set_msg("<p>Nenhuma alteração efetuada</p>");
                }
            }

        }

        $dados_cabecalho['titulo_pagina'] = 'Edição de Notícias';
        $dados['tela'] = 'editar';
        $dados['noticias'] = $this->noticia_model->get();
        $this->load->view('painel/init_cabecalho', $dados_cabecalho);
        $this->load->view('painel/noticias', $dados);
        $this->load->view('painel/init_rodape');
    } // end of method

} // end of file