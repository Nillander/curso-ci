<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//seta uma mensagem via session para ser lida posteriormente
if(!function_exists('set_msg')):
	function set_msg($msg=NULL){
		$ci = & get_instance();
		$ci->session->set_userdata('aviso', $msg);
	}
endif;

//retorna uma mensagem definida pela função set_msg
if(!function_exists('get_msg')):
	function get_msg($destroy=TRUE){
		$ci = & get_instance();
		$retorno = $ci->session->userdata('aviso');
		if($destroy) $ci->session->unset_userdata('aviso');
		return $retorno;
	}
endif;

//verifica se o usuário está logado, caso negativa redireciona para outra página
if(!function_exists('verifica_login')):
	function verifica_login($redirect='setup/login') {
		$ci = & get_instance();
		if($ci->session->userdata('logged') != true){
			set_msg('<p>Acesso Restrito! Faça login para continuar</p>');
			redirect($redirect,'refresh');
		}
	}
endif;

//define as configurações para upload de imagens/arquivos
if(!function_exists('config_upload')){
	function config_upload($path='./uploads/',$types='jpg|png', $size=512){
		$config['upload_path'] = $path;
		$config['allowed_types'] = $types;
		$config['max_size'] = $size;
		return $config;
	}
}

// codifica o html
if(!function_exists('to_bancodedados')){
	function to_bancodedados($string=NULL){
		return htmlentities($string);
	}
}

// descodifica o html
if(!function_exists('to_html')){
	function to_html($string=NULL){
		return html_entity_decode($string);
	}
}

